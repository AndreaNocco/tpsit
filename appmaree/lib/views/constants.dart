import 'package:flutter/material.dart';

final Color primaryTextColor = Color(0xFF54c3ff);
final Color backgroundColor = Color(0xFFffffff);
final Color waveColor = Colors.teal;
