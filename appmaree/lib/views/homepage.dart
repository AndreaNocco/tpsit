import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maree/cubit/tide_cubit.dart';
import 'package:maree/models/tide.dart';
import 'package:maree/views/constants.dart';
import 'package:maree/widgets/buildin_transform.dart';
import 'package:maree/widgets/single_tide.dart';
import 'package:maree/widgets/slider_dot.dart';
import 'package:maree/widgets/wave_widget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:transformer_page_view/transformer_page_view.dart';

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  int _currentPage = 0;
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  @override
  void initState() {
    getData();
    super.initState();
  }

  _onPageChanged(int index) {
    setState(() {
      _currentPage = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Text(''),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      backgroundColor: backgroundColor,
      body: SmartRefresher(
        enablePullDown: true,
        controller: _refreshController,
        onRefresh: getData,
        child: Container(
          alignment: Alignment.center,
          child: BlocBuilder<TideCubit, TideState>(
            builder: (context, state) {
              if (state is TideInitial) {
                return buildInitial();
              } else if (state is TideLoading) {
                return buildLoading();
              } else if (state is TideLoaded) {
                return buildLoaded(state.tides);
              } else {
                return buildError();
              }
            },
          ),
        ),
      ),
    );
  }

  Widget buildInitial() {
    return Container();
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildLoaded(List<Tide> tide) {
    final size = MediaQuery.of(context).size;
    return Stack(
      children: [
        Align(
          alignment: Alignment.bottomCenter,
          child: WaveWidget(
            size: size,
            yOffset: size.height / 2.0,
            xOffset: 40,
            color: Colors.teal[200],
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: WaveWidget(
            size: size,
            yOffset: size.height / 1.975,
            xOffset: 30,
            color: Colors.teal,
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.all(28.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                for (int i = 0; i < tide.length; i++)
                  if (i == _currentPage) SliderDot(true) else SliderDot(false)
              ],
            ),
          ),
        ),
        TransformerPageView(
          viewportFraction: 0.8,
          scrollDirection: Axis.horizontal,
          transformer: AccordionTransformer(),
          itemCount: tide.length,
          onPageChanged: _onPageChanged,
          itemBuilder: (ctx, i) => SingleTide(tide[i]),
        ),
      ],
    );
  }

  Widget buildError() {
    return Center(
      child: Text("An error occurred while loading the page..."),
    );
  }

  void getData() {
    final tideCubit = context.read<TideCubit>();
    tideCubit.getData();
    _currentPage = 0;
    _refreshController.refreshCompleted();
  }
}
