import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maree/cubit/tide_cubit.dart';
import 'package:maree/repositories/tide_repository.dart';
import 'package:maree/views/homepage.dart';

void main() {
  runApp(Bootstrap());
}

class Bootstrap extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Tide App',
      home: BlocProvider(
        create: (context) => TideCubit(TideRepository()),
        child: Homepage(),
      ),
    );
  }
}
