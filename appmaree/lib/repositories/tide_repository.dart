import 'dart:convert';

import 'package:maree/models/tide.dart';
import 'package:http/http.dart' as http;

abstract class AbstractTideRepository {
  Future<List<Tide>> fetchData();
}

class TideRepository implements AbstractTideRepository {
  @override
  Future<List<Tide>> fetchData() async {
    final response = await http.get(
      Uri.parse(
          'https://dati.venezia.it/sites/default/files/dataset/opendata/livello.json'),
    );

    if (response.statusCode == 200) {
      return jsonDecode(response.body)
          .map<Tide>((json) => Tide.fromJson(json))
          .toList();
    } else {
      throw Exception('Failed to get Data');
    }
  }
}
