part of 'tide_cubit.dart';

@immutable
abstract class TideState {
  const TideState();
}

class TideInitial extends TideState {
  const TideInitial();
}

class TideLoading extends TideState {
  const TideLoading();
}

class TideLoaded extends TideState {
  final List<Tide> tides;
  const TideLoaded(this.tides);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is TideLoaded && o.tides == tides;
  }

  @override
  int get hashCode => tides.hashCode;
}

class TideError extends TideState {
  final String message;
  const TideError(this.message);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is TideError && o.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}
