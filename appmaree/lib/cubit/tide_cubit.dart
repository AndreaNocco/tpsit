import 'package:bloc/bloc.dart';
import 'package:maree/models/tide.dart';
import 'package:maree/repositories/tide_repository.dart';
import 'package:meta/meta.dart';

part 'tide_state.dart';

class TideCubit extends Cubit<TideState> {
  final TideRepository _tideRepository;
  TideCubit(this._tideRepository) : super(TideInitial());

  Future<void> getData() async {
    try {
      // emit loading state
      emit(
        TideLoading(),
      );
      final tides = await _tideRepository.fetchData();
      // emit loaded state
      emit(
        TideLoaded(tides),
      );
    } on Exception {
      // emit error state
      emit(
        TideError("Couldn't fetch tides. Is the device online?"),
      );
    }
  }
}
