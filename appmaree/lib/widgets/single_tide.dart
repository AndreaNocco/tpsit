import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:maree/models/tide.dart';
import 'package:maree/views/constants.dart';

class SingleTide extends StatelessWidget {
  final Tide tide;
  SingleTide(this.tide);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20, top: 150),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          tide.valoreDouble.toString(),
                          style: GoogleFonts.openSans(
                            fontSize: 80,
                            fontWeight: FontWeight.w300,
                            color: waveColor,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          'Metri',
                          style: GoogleFonts.openSans(
                            fontSize: 30,
                            fontWeight: FontWeight.w300,
                            color: waveColor,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Stazione: ',
                      style: GoogleFonts.openSans(
                        fontSize: 20,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      tide.stazione,
                      style: GoogleFonts.openSans(
                        fontSize: 35,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                  ],
                ),
              ],
            ),
          ),
          Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(vertical: 40),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.white30,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
