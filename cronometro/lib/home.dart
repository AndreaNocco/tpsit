import 'dart:async';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  int _ore, _minuti, _secondi;
  String _tempo = '00:00:00';
  Stream<int> _stream;
  StreamSubscription _streamSubscription;
  AnimationController _pausa;

  @override
  void initState() {
    super.initState();
    _stream = Stream.periodic(Duration(seconds: 1), (callBack) => callBack);
    _ore = _minuti = _secondi = 0;

    _streamSubscription = _stream.listen((event) {
      if (_secondi == 59) {
        if (_minuti == 59) {
          setState(() {
            _secondi = _minuti = 0;
            _ore++;
          });
        } else {
          setState(() {
            _secondi = 0;
            _minuti++;
          });
        }
      } else {
        setState(() {
          _secondi++;
        });
      }
      setState(() {
        _tempo = times();
      });
    });
    _streamSubscription.pause();

    _pausa =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));
  }

  void pause() {
    if (_streamSubscription.isPaused) {
      setState(() {
        _streamSubscription.resume();
        _pausa.forward();
      });
    } else {
      setState(() {
        _streamSubscription.pause();
        _pausa.reverse();
      });
    }
  }

  String times() {
    String ore, minuti, secondi;

    ore = _ore < 10 ? '0$_ore' : '$_ore';
    minuti = _minuti < 10 ? '0$_minuti' : '$_minuti';
    secondi = _secondi < 10 ? '0$_secondi' : '$_secondi';

    return "$ore:$minuti:$secondi";
  }

  void delete() {
    if (_streamSubscription.isPaused) {
      setState(() {
        _ore = _minuti = _secondi = 0;
        _tempo = '00:00:00';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child:Text('CRONOMETRO'),
        )

      ),
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(
                _tempo,
                style: TextStyle(fontSize: 50.0, color: Colors.grey),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(

                    decoration: BoxDecoration(
                      shape:BoxShape.circle,
                      color:Colors.grey,
                    ),
                    child: IconButton(
                      iconSize: 50.0,
                      icon: AnimatedIcon(
                        progress: _pausa,
                        icon: AnimatedIcons.play_pause,
                        color: Colors.white,
                      ),
                      onPressed: () => pause(),
                    ),
                  ),

                  Container(

                    decoration: BoxDecoration(
                      shape:BoxShape.circle,
                      color:Colors.grey,
                    ),
                    child: IconButton(
                      iconSize: 50.0,
                      icon: Icon(Icons.delete, color: Colors.white),
                      onPressed: () => delete(),
                    )
                  ),

                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
