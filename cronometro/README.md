Funzionamento:
initState()
Nel metodo principale initState() è presente uno stream periodic il quale si aggiorna ad ogni secondo ed uno stream subscription che lo ascolta
se l'int dedicato ai secondi ed ai minuti raggiunge il valore "59" allora esso viene portato a zero e l'int delle ore viene aumentato di uno.
Se invece solo l'int dei secondi raggiunge il valore "59" allora esso viene portato a zero e l'int dei minuti viene aumentato di uno.
Per qualsiasi altro caso l'int dei secondi viene aumentato di uno ad ogni ciclo dello stream.
<pre><code> void initState() {
    super.initState();
    _stream = Stream.periodic(Duration(seconds: 1), (callBack) => callBack);
    _ore = _minuti = _secondi = 0;

    _streamSubscription = _stream.listen((event) {
      if (_secondi == 59) {
        if (_minuti == 59) {
          setState(() {
            _secondi = _minuti = 0;
            _ore++;
          });
        } else {
          setState(() {
            _secondi = 0;
            _minuti++;
          });
        }
      } else {
        setState(() {
          _secondi++;
        });
      }
      setState(() {
        _tempo = times();
      });
    });
    _streamSubscription.pause();

    _pausa =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));
  }
</pre></code>
pause()
Il metodo pause() viene invocato solo quando viene premuto il pulsante pausa/play e si occupa di mettere lo stream in pausa quando non è gia in pausa e di riprenderlo se invece prima era in pausa.
<pre><code>void pause() {
    if (_streamSubscription.isPaused) {
      setState(() {
        _streamSubscription.resume();
        _pausa.forward();
      });
    } else {
      setState(() {
        _streamSubscription.pause();
        _pausa.reverse();
      });
    }
  }</pre></code>
times()
Il metodo times() ritorna una stringa la quale contiene le stringhe "ore","minuti","secondi" unite assieme.Permette di mantenere una buona estetica del cronometro nell'applicazione.
<pre><code>String times() {
    String ore, minuti, secondi;

    ore = _ore < 10 ? '0$_ore' : '$_ore';
    minuti = _minuti < 10 ? '0$_minuti' : '$_minuti';
    secondi = _secondi < 10 ? '0$_secondi' : '$_secondi';

    return "$ore:$minuti:$secondi";
  }</pre></code>
delete()
Il metodo delete viene invocato quando viene premuto il pulsante "delete" ma funziona solo quando il cronometro è in pausa.Permette di azzerare i valori degli int dedicati alle ore,minuti e secondi riportando il cronometro al suo stato iniziale.
<pre><code>void delete() {
    if (_streamSubscription.isPaused) {
      setState(() {
        _ore = _minuti = _secondi = 0;
        _tempo = '00:00:00';
      });
    }
  }</pre></code>