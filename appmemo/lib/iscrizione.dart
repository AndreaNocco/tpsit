import 'package:appmemo/notes.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:google_sign_in/google_sign_in.dart';
//import 'email_iscrizione.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final String title = "AppMemo";
  final _googleSignIn = GoogleSignIn();
  final _auth = FirebaseAuth.instance;
  bool _loading = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(this.title),
        ),
        body: Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <
              Widget>[
            Padding(
              padding: EdgeInsets.all(10.0),
              child: Text("Benvenuto!",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 30,
                      fontFamily: 'Roboto')),
            ),
            Padding(
                padding: EdgeInsets.all(10.0),
                child: SignInButton(
                  Buttons.Google,
                  text: "Continua con Google",
                  onPressed: () {
                    _signInWithGoogle(context).then((value) {
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => NotesPage(
                                uid: value,
                              )));
                    });
                  },
                )),
            /*Padding(
                padding: EdgeInsets.all(10.0),
                child: SignInButton(
                  Buttons.Email,
                  text: "Continua con Email",
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => EmailSignUp()),
                    );
                  },
                )),*/
          ]),
        ));
  }

  Future<String> _signInWithGoogle(BuildContext context) async {
    try {
      setState(() => _loading = true);

      final googleUser = await _googleSignIn.signIn();
      final googleAuth = await googleUser.authentication;
      final credential = GoogleAuthProvider.credential(
        idToken: googleAuth.idToken,
        accessToken: googleAuth.accessToken,
      );
      final userCredential = await _auth.signInWithCredential(credential);

      await FirebaseFirestore.instance
          .collection('users')
          .doc(userCredential.user.uid)
          .set(
        {
          'email': userCredential.user.email,
          'name': userCredential.user.displayName,
          'memos': FieldValue.arrayUnion([]),
        },
      );
      return userCredential.user.uid;
    } catch (e, s) {
      print(e);
    } finally {
      setState(() => _loading = false);
    }
  }
}
