# Descrizione dell'AppMemo
+ **Server:** Ho deciso di utilizzare Firebase che mi permette di fare l'accesso tramite account Google.
+ **Memo:** Il memo visualizzato nelle varie ListView è un widget creato nell'apposito file dart *notes.dart*.
+ **Struttura Firestore**: All'interno di Firestore è presente una raccolta utenti che contiene un'altra raccolta che ha come nome l'email dell'utente che esegue l'accesso.
+ **Memo:** Il memo è un documento presente dentro la raccolta *memo* che ha come campi:
  + La stringa **corpo**.
  + La stringa **titolo**.
+ **Funzioni:** Per le funzioni di eliminazione e salvataggio si ha utilizzato un pacchetto che permette di eseguire determinate azioni "trascinando" il memo in una certa direzione.
+ **Eliminare i memo:** É possibile elimare i memo direttamente dalla HomePage "trascinando" il memo che si vuole modificare da sinistra verso destra, e selezionare dal menù che compare la voce Elimina.
+ **Creazione memo:** Si può creare il memo premendo il bottone situato in basso a destra presente nella HomePage, si verrà reinderizzata alla pagina Nuovo Memo nella quale è possibile inserire:
    + Titolo.
    + Testo.
# appmemo
Vi presento l'AppMemo!
L'app che ho creato si basa sull'estrarre i dati dal database online (firebase) ed inserirle, all'apertura dell'app, nella schermata principale.

## Autenticazione 
Per permettere l'autenticazione nell'applicazione bisogna, come prima passo, abilitare nella console firebase. Successivamente è necessario aggiungere il plug-in a pubspec.yaml:
```
dependencies:
  flutter:
    sdk: flutter
  firebase_core: ^0.5.0+1
  firebase_database: ^4.1.1
  firebase_auth: ^0.18.1+2
  splashscreen : 1.2.0
  flutter_signin_button: ^1.0.0
  flutter_svg:
  google_sign_in:
  cloud_firestore:
  auto_size_text:
```
### Creazione della pagina iniziale
Andiamo a creare all'interno del main.dart un statelesswidget:
```
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'App Memo',
      theme: ThemeData(
        primaryColor: Colors.indigo.shade600,
      ),
      home: IntroScreen(),
    );
  }
}
```
Subito dopo andiamo a creare un metodo IntroScreen():
```
class IntroScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    User result = FirebaseAuth.instance.currentUser;
    return new SplashScreen(
        navigateAfterSeconds: result != null ? NotesPage() : SignUp(),
        seconds: 5,
        title: new Text(
          "Benvenuto nell' App Memo!",
          style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
        ),
        // image: Image.asset('assets/images/dart.png', fit: BoxFit.scaleDown),
        backgroundColor: Colors.white,
        styleTextUnderTheLoader: new TextStyle(),
        photoSize: 100.0,
        onClick: () => print("flutter"),
        loaderColor: Colors.red);
  }
}
```
Lo splashscreen lo utilizziamo per creare una schermata iniziale.

### Pulsanti per la registrazione

Nella pagina iniziale "AppMemo" creiamo due pulsanti: uno per registrarti utilizzando Google e l'altro tramite l'email. Questo è possibile scrivendo tale metodo:
```
return Scaffold(
        appBar: AppBar(
          title: Text(this.title),
        ),
        body: Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <
              Widget>[
            Padding(
              padding: EdgeInsets.all(10.0),
              child: Text("Benvenuto!",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 30,
                      fontFamily: 'Roboto')),
            ),
            Padding(
                padding: EdgeInsets.all(10.0),
                child: SignInButton(
                  Buttons.Google,
                  text: "Continua con Google",
                  onPressed: () {
                    _signInWithGoogle(context).then((value) {
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => NotesPage(
                                uid: value,
                              )));
                    });
                  },
                )),
            Padding(
                padding: EdgeInsets.all(10.0),
                child: SignInButton(
                  Buttons.Email,
                  text: "Continua con Email",
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => EmailSignUp()),
                    );
                  },
                )),
          ]),
        ));
```
All'interno andiamo ad usare la classe SignInButton che creerà diversi pulsanti di accesso personalizzati.
