import 'package:memoapp/signin_button.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'email_login.dart';
import 'email_signup.dart';
import 'home.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final String title = "Registrati";
  final _googleSignIn = GoogleSignIn();
  final _auth = FirebaseAuth.instance;

  bool _loading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: Center(
        child: _loading
            ? CircularProgressIndicator()
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text("Benvenuto",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 30,
                            fontFamily: 'Pacifico')),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10.0),
                    child: GoogleButton(
                      "Accedi con Gooogle",
                      onTap: () => _signInWithGoogle(context),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10.0),
                    child: GestureDetector(
                      child: Text(
                        "Fai il Log In con la Email ->",
                        style: TextStyle(
                          //decoration: TextDecoration.underline,
                          color: Colors.blueGrey,
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => EmailLogIn()),
                        );
                      },
                    ),
                  )
                ],
              ),
      ),
    );
  }

  void _signInWithGoogle(BuildContext context) async {
    try {
      setState(() => _loading = true);

      final googleUser = await _googleSignIn.signIn();
      final googleAuth = await googleUser.authentication;
      final credential = GoogleAuthProvider.credential(
        idToken: googleAuth.idToken,
        accessToken: googleAuth.accessToken,
      );
      final userCredential = await _auth.signInWithCredential(credential);

      await FirebaseFirestore.instance
          .collection('users')
          .doc(userCredential.user.uid)
          .set(
        {
          'email': userCredential.user.email,
          'name': userCredential.user.displayName,
          'memos': FieldValue.arrayUnion([]),
        },
      );
Navigator.of(context).push(MaterialPageRoute(builder:(context) => Home(uid: userCredential.user.uid)));
    } catch (e, s) {
      print(e);
    } finally {
      setState(() => _loading = false);
    }
  }
}
