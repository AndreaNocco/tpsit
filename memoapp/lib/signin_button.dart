import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';


/// Button in the login and registration with facebook
class GoogleButton extends StatelessWidget {
  final String text;
  final GestureTapCallback onTap;

  const GoogleButton(
    this.text, {
    Key key,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        padding: const EdgeInsets.symmetric(
          vertical: 16,
          horizontal: 16,
        ),
        primary: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
      ),
      child: Row(
        children: <Widget>[
          const SizedBox(
            width: 17,
          ),
          SvgPicture.asset('assets/google.svg'),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Container(
              width: 0.5,
              height: 32,
              decoration: BoxDecoration(
                color: Colors.black,
              ),
            ),
          ),
          Expanded(
            child: Text(
              text,
              style: TextStyle(
                fontSize: 15,
                color:Colors.black,
                fontWeight: FontWeight.normal,
              ),
            ),
          ),
        ],
      ),
      onPressed: onTap,
    );
  }
}